/**
 * Created by pedro.rueda on 25/10/2017.
 */
const Model = {};

const user = {
  save: function () {
    $.post('/challege', Model);
  }
};

const App = {
  init: function () {
    this.cacheElements();
    this.bindEvents();
  },
  cacheElements: function () {
    $save = $("#save");
  },
  bindEvents: function() {
    $('input').on('keyup', App.updateModel);
    $('#save').on('click', user.save)
  },
  updateModel: function ( evt ) {
    Model[$(this).data("model")] = evt.target.value;
    console.log($(this), $(this).data("model"), Model);
  }
};


App.init();
