# sails-react-bootcamp

 A [Sails](http://sailsjs.org) application.
 The main goal of this bootcamp is to show you how to build a production ready app.
 For this you have to learn an entire cycle of application  development in a practical way:

 from an idea -> to design  -> to boilerplate -> to development -> to deployment

 We want to show you how that cycle is a endless process. It starts with an idea to improve an existing application,
 and ends with deploying a new version of the application containing its implementation.

 We are going to create a React app served from a Sails Project with an authentication process, this React app will be connected
 to firebase as a presentation layer of our data. Finally we are going to deploy our application on an heroku dev server in order to show how
 the build and deploy process works and we will see some basics automation concepts to make it easier.


1. First you have to familiarize with Node.Js simple doing a .js script and run it to play a little bit as in a browser.

2. you have to  get acquainted with npm package manager, lets do a new project folder and try some commands

   $: npm init

3. have fun installing several packages with

   $: npm install <package name> [--save --save-dev] (lets install Hapi.js and make a quick app server. return strings or files with it)

4. its good to know about facebook yarn as an alternative to npm.

   $: yarn add [--dev] commands

5. from here on we are going to make an app that has several features to manage your Code Challenge application.

# Server Project

we need an app server to interact with. for that we are going to work with Sails JS, you can see some documentation here:

http://sailsjs.com/documentation

Sails is a back-end code generator Framework for node built up on top of Express instead of hapi. It has a great api to work with
back end code without getting a headache. It even has an ORM (waterline) to avoid spending time fighting with DB stuff.

so first lets install sails:

   $: npm install sails -g

the "-g" flag indicates npm to install the package on a "global" way to be accessed from any location from your PATH.

then create a new app with it

   $: sails new sails-react-bootcamp

from here you can see the changes we will make to the code on this repo, if you want you can clone it for faster understanding.

   $: cd sails-react-bootcamp

here you have a good explanation of the structure of the generated code:

http://sailsjs.com/documentation/anatomy

lets start to see our app working...

   $: sails lift

go to browser and open:

http://localhost:3000

it will show an example view with a minimal api configuration.



