/**
 * Challege.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    title: {
      type: 'string',
      required: true,
      unique: true
    },
    description: {
      type: 'string',
      required: true
    },
    js: {
      type: 'string'
    },
    html: {
      type: 'string'
    },
    css: {
      type: 'string'
    },
    solve: {
      type: 'array',
      defaultsTo: []
    }
  },
  beforeCreate: function ( challenge, cb ) {
    if ( challenge.js ) challenge.solve.push(challenge.js);
    if ( challenge.html ) challenge.solve.push(challenge.html);
    if ( challenge.css ) challenge.solve.push(challenge.css);
    cb();
  }
};

